<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\User;
use App\Models\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
            Category::factory(6)->create();
            User::factory(6)->create();
            for ($i = 0; $i < 20; $i++) {
                Post::factory()->create([
                    'user_id' => rand(1, User::count()),
                    'category_id' => rand(1, Category::count())
                ]);
            }
    }
}
