<?php

use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Spatie\YamlFrontMatter\YamlFrontMatter;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index'])->name('home');

Route::get('posts/{post:slug}', function(Post $post){
    return view('post', ["post" => $post ]);
});

Route::get('category/{category:slug}', function(Category $category) {
    return view('posts', [
        'posts' => $category->posts->load(['category', 'user']),
        'currentCategory' => $category,
        'categories' => Category::all()
    ]);
});

Route::get('user/{user:username}', function(User $user) {
    return view('posts', [
        'posts' => $user->posts->load(['category', 'user'])
    ]);
});
