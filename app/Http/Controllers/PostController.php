<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{
    public function index()
    {
      $posts = Post::latest();
      if (request('search')) {
        $posts->where('title','like', '%' . request('search') . '%');
      }


      return view('posts', [
        'posts' => $posts->get(),
        'currentCategory' => false,
        'categories' => Category::all()
      ]);

    }
}
